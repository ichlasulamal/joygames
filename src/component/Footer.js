import React from 'react'
import image1 from '../community/Vector.png'
import image2 from '../community/Vector1.png'
import image3 from '../community/Vector2.png'
import image4 from '../community/Vector3.png'
import image5 from '../community/Vector4.png'
import MyLogo from './MyLogo'



const Footer = () => {
  return (
    <div className='container footer pt-5 mt-5 '>

        <div className='row pb-5
        '>
            <div className='col-4'>
                <div style={{width: '220px', marginBottom: '10px'}}>

                    <MyLogo />
                </div>
                <p style={{fontWeight: '700', margin: 0}}>METAVERSE GAMING</p>
                <p style={{fontWeight: '600'}}>BY JOY GAME PLAYER</p>

            </div>
            <div className='col-2'>
                <p className='footer-title'>OUR SERVICES</p>
                <p>ABOUT</p>
                <p>STORE</p>
                <p>GAMEBOX</p>
                <p>TOKEN</p>
                <p>STACKING</p>
                <p>DOCS</p>

            </div>
            <div className='col-2'>
                <p className='footer-title'>HELP</p>
                <p>HELPS</p>
                <p>FAQs</p>
                <p>CONTACT</p>


            </div>
            <div className='col-4'>
                <p className='footer-title'>JOIN OUR COMMUNITY</p>
                <div className='row'>
                    <div className='col-2'>
                        <img src={image1} alt="" width="30" height="24" class="d-inline-block align-text-top" />
                    </div>
                    <div className='col-2'>
                        <img src={image2} alt="" width="30" height="24" class="d-inline-block align-text-top" />
                    </div>
                    <div className='col-2'>
                        <img src={image3} alt="" width="30" height="24" class="d-inline-block align-text-top" />
                    </div>
                    <div className='col-2'>
                        <img src={image4} alt="" width="30" height="24" class="d-inline-block align-text-top" />
                    </div>
                    <div className='col-2'>
                        <img src={image5} alt="" width="30" height="24" class="d-inline-block align-text-top" />
                    </div>


                </div>

            </div>

        </div>
        <div class="row justify-content-md-center mt-5">

            <div style={{ textAlign: 'center'}} class="col col-8">
                <p><b>2022 Joy Game | Term & Condition | </b> Privacy Policy</p>
            </div>
        </div>

    </div>
  )
}

export default Footer