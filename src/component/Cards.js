import React from 'react';
import image1 from '../image1.png'
import image2 from '../image2.png'
import image3 from '../image3.png'
import image4 from '../image4.png'
import image5 from '../image5.png'
import image6 from '../image6.png'

const Cards = () => {
  const data  = [
      {
          'judul': 'Claw Starts',
          'description' : 'Enter the world of Masketeers where brave heroes empowered by mysterious masks take a stand against the inner demons of society. Soon you will be able to adorn yourself with exclusive NFT Costumes that can empower your Masketeers as they unleash their attacks on the manifestations of negativity.',
          'image_src' : 'image1'
      },
      {
        'judul': 'Masketeers',
        'description' : 'Enter the world of Masketeers where brave heroes empowered by mysterious masks take a stand against the inner demons of society. Soon you will be able to adorn yourself with exclusive NFT Costumes that can empower your Masketeers as they unleash their attacks on the manifestations of negativity.',
        'image_src' : 'image2'
      },
      {
        'judul': 'The Last',
        'description' : 'Enter the world of Masketeers where brave heroes empowered by mysterious masks take a stand against the inner demons of society. Soon you will be able to adorn yourself with exclusive NFT Costumes that can empower your Masketeers as they unleash their attacks on the manifestations of negativity.',
        'image_src' : 'image3'
      },
      {
        'judul': 'Moonland',
        'description' : 'Enter the world of Masketeers where brave heroes empowered by mysterious masks take a stand against the inner demons of society. Soon you will be able to adorn yourself with exclusive NFT Costumes that can empower your Masketeers as they unleash their attacks on the manifestations of negativity.',
        'image_src' : 'image4'
      },
      {
        'judul': 'Axie Infinity',
        'description' : 'Enter the world of Masketeers where brave heroes empowered by mysterious masks take a stand against the inner demons of society. Soon you will be able to adorn yourself with exclusive NFT Costumes that can empower your Masketeers as they unleash their attacks on the manifestations of negativity.',
        'image_src' : 'image5'
      },
      { 
        'judul': 'Kimetzu hero',
        'description' : 'Enter the world of Masketeers where brave heroes empowered by mysterious masks take a stand against the inner demons of society. Soon you will be able to adorn yourself with exclusive NFT Costumes that can empower your Masketeers as they unleash their attacks on the manifestations of negativity.',
        'image_src' : 'image6'
      }
  ]
  return <div className='row'>
    <div className='col col-4 p-3'>
      <div className='mycard'  style={{ display : 'flex', flexDirection: 'column', justifyContent: 'end', height: '350px', background: `url(${image1})`}}>
        <p className='textMyCard' style={{border: '2px solid black', alignItems: 'center', margin: 0, padding: "20px 10px", background: 'black', opacity: '0.8'}}>{data[0].judul}</p>
      </div>
    </div>
    <div className='col col-4 p-3'>
      <div className='mycard'  style={{ display : 'flex', flexDirection: 'column', justifyContent: 'end', height: '350px', background: `url(${image2})`}}>
        <p className='textMyCard' style={{border: '2px solid black', alignItems: 'center', margin: 0, padding: "20px 10px", background: 'black', opacity: '0.8'}}>{data[1].judul}</p>
      </div>
    </div>
    <div className='col col-4 p-3'>
      <div className='mycard'  style={{ display : 'flex', flexDirection: 'column', justifyContent: 'end', height: '350px', background: `url(${image3})`}}>
        <p className='textMyCard' style={{border: '2px solid black', alignItems: 'center', margin: 0, padding: "20px 10px", background: 'black', opacity: '0.8'}}>{data[2].judul}</p>
      </div>
    </div>
    <div className='col col-4 p-3'>
      <div className='mycard'  style={{ display : 'flex', flexDirection: 'column', justifyContent: 'end', height: '350px', background: `url(${image4})`}}>
        <p className='textMyCard' style={{border: '2px solid black', alignItems: 'center', margin: 0, padding: "20px 10px", background: 'black', opacity: '0.8'}}>{data[3].judul}</p>
      </div>
    </div>
    <div className='col col-4 p-3'>
      <div className='mycard'  style={{ display : 'flex', flexDirection: 'column', justifyContent: 'end', height: '350px', background: `url(${image5})`}}>
        <p className='textMyCard' style={{border: '2px solid black', alignItems: 'center', margin: 0, padding: "20px 10px", background: 'black', opacity: '0.8'}}>{data[4].judul}</p>
      </div>
    </div>
    <div className='col col-4 p-3'>
      <div className='mycard'  style={{ display : 'flex', flexDirection: 'column', justifyContent: 'end', height: '350px', background: `url(${image6})`}}>
        <p className='textMyCard' style={{border: '2px solid black', alignItems: 'center', margin: 0, padding: "20px 10px", background: 'black', opacity: '0.8'}}>{data[5].judul}</p>
      </div>
    </div>

     
  </div>;
};

export default Cards;


