import React from 'react';
import '../App.css';
import MyLogo from './MyLogo';
// import logo from './logo.svg';


const Header = () => {
    const data = ['ABOUT','STORE','GAMEBOX','TOKEN', 'STACKING','DOCS']

  return <div>
            <nav style={{opacity:0.8}} class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark px-5">
                <div class="container-fluid">

                    <MyLogo/>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
      
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0 mx-5">
                            {data.map((e) => { 
                                return (
                                    <li class="nav-item header-text">
                                        <a class="nav-link active" aria-current="page" href="/">{e}</a>
                                    </li>

                                )

                            })}
                        </ul>
                        
                    </div>
                </div>
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/">REGISTER</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-primary  px-5" href="/">LOGIN</a>
                    </li>
                </ul>
                </nav>
  </div>;
};

export default Header;
