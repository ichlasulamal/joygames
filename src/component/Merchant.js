import React from 'react';
import image1 from '../image_54.png';
import image2 from '../image_52.png';
import image3 from '../image_50.png';
import image4 from '../image_51.png';

const Merchant = () => {
    return <div className='merchants p-5 '>
       
        <div style={{textAlign: 'center'}} class="row justify-content-md-center align-items-center px-5">
             <div class="col col-lg-3">
                <img src={image1}  alt="/" />
    
            </div>
            <div class="col col-lg-3">
                <img src={image2}  alt="/" />
    
            </div>
            <div class="col col-lg-3 ">
                <img src={image3}  alt="/" />

    
            </div>
            <div class="col col-lg-3">
                <img src={image4}  alt="/" />
    
            </div>
        </div>
        
    </div>;
};

export default Merchant;
