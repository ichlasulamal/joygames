import React from 'react';
// import logo from '../Group_99.svg'
import '../App.css';
import image_opening from '../image_opening.png'
import MyLogo from './MyLogo';

const Opening = () => {
  return <div className='opening p-5 row justify-content-md-center'>
        <div style={{width: '220px', marginBottom: '10px'}}>

            <MyLogo />
        </div>
        <div className='row justify-content-md-center align-items-center mt-0' style={{marginTop: '-50px'}}>
            
            <div class="col-3" style={{textAlign: 'left'}}>
                <h4>
                    INTO THE METAVERSE
                </h4>
                <p className='paragrap'>Earn, trade, stake, farm and swap your game assets the way you want. Welcome to a metaverse that lets you truly decide how you want to play.</p>
            </div>
            <div class="col-6">
                {/* <video width="614" height="345" poster={image_opening} >
                    <source src="movie.mp4" type="video/mp4" />
                </video> */}
                {/* <iframe width="614" height="345" poster={image_opening} 
                    src="https://www.youtube.com/embed/tgbNymZ7vqY">
                </iframe> */}
                <img style={{height:'345px'}} src={image_opening} alt='/'/>
            </div>
            <div class="col-3" style={{textAlign: 'justify'}}>
                <p className='paragrap'>This is for all the gamers out there no matter who you are, whether you're a newbie or a whale. It's time to game on your own terms. Welcome to Joy Games - Metaverse gaming by true game makers.</p>
            </div>

        </div>
  </div>;
};

export default Opening;
