import React from 'react';
import Cards from './Cards';


const Upcoming = () => {
  const data = ['Adventure', 'Arcade', 'Race', 'Open World','RPG','MMRPG','FPS','Multiplayer']

  return <div className='upcoming'>
      <div class="row justify-content-md-center mb-5">
         <div style={{ textAlign: 'center'}} class="col col-8">
            <p className='upcomingTitle m-5'>Upcoming Games</p>
            <p>
              Joy Games aims to not just create a platform but also will play an active role in the development and curation of gaming content on the platform, the upkeep of the codebase, and the development of new exciting ways to implement gamified finance into existing or new games, via a mixture of of inhouse development or co-development with our strategic partners, who are experienced and successful game development studios.
              </p>
         </div>
      </div>
      <div class="container pb-5" style={{textAlign: 'center'}}>
        <div class="row mb-5" >
            {data.map(e=>{
                return (
                    <div class="col col-3">
                        <p style={{borderRadius: '20px', textAlign: 'center', background: '#5D5FEF', fontWeight: '700'}} className='py-3'>
                            {e}
                        </p>
                    </div>
                )
            })}
            

        </div>
        <div class="row justify-content-md-center mb-5">
            <Cards />
        </div>

        <div className='btn btn-primary px-4' >VIEW MORE</div>

        

      </div>
  </div>;
};

export default Upcoming;
