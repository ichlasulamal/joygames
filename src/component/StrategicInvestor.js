import React from 'react'
import image1 from '../merchant/image1.png'
import image2 from '../merchant/image2.png'
import image3 from '../merchant/image3.png'
import image4 from '../merchant/image4.png'
import image5 from '../merchant/image5.png'
import image6 from '../merchant/image6.png'
import image7 from '../merchant/image7.png'
import image8 from '../merchant/image8.png'
import image9 from '../merchant/image9.png'
import image10 from '../merchant/image10.png'
import image11 from '../merchant/image11.png'
import image12  from '../merchant/image12.png'
import image13 from '../merchant/image13.png'
import image14 from '../merchant/image14.png'
import image15 from '../merchant/image15.png'
import image16 from '../merchant/image16.png'



const StrategicInvestor = () => {
  return (
    <div className='strategy'>
        <div class="row justify-content-md-center mb-5">
         <div style={{ textAlign: 'center'}} class="col col-8">
            <p className='upcomingTitle mt-5'>STRATEGIC INVESTOR</p>
         </div>
      </div>
      <div style={{textAlign: 'center'}} class="row justify-content-md-center align-items-center px-5">
        <div class="col col-lg-3">
            <img src={image1}  alt="/" />
        </div>
        <div class="col col-lg-3">
            <img src={image2}  alt="/" />
        </div>
        <div class="col col-lg-3">
            <img src={image3}  alt="/" />
        </div>
        <div class="col col-lg-3">
            <img src={image4}  alt="/" />
        </div>
        <div class="col col-lg-3">
            <img src={image5}  alt="/" />
        </div>
        <div class="col col-lg-3">
            <img src={image6}  alt="/" />
        </div>
        <div class="col col-lg-3">
            <img src={image7}  alt="/" />
        </div>
        <div class="col col-lg-3">
            <img src={image8}  alt="/" />
        </div>
        <div class="col col-lg-3">
            <img src={image9}  alt="/" />
        </div>
        <div class="col col-lg-3">
            <img src={image10}  alt="/" />
        </div>
        <div class="col col-lg-3">
            <img src={image11}  alt="/" />
        </div>
        <div class="col col-lg-3">
            <img src={image12}  alt="/" />
        </div>
        <div class="col col-lg-3">
            <img src={image13}  alt="/" />
        </div>
        <div class="col col-lg-3">
            <img src={image14}  alt="/" />
        </div>
        <div class="col col-lg-3">
            <img src={image15}  alt="/" />
        </div>
        <div class="col col-lg-3">
            <img src={image16}  alt="/" />
        </div>
      </div>
    </div>
  )
}

export default StrategicInvestor