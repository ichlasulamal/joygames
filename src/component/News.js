import React from 'react'
import image1 from '../slider/slider1.png'
import './News.css'
// Commonjs
const Flickity = require('react-flickity-component');
const flickityOptions = {
    initialIndex: 0
}

function Carousel() {
  return (
    <Flickity
      className={'carousel'} // default ''
      elementType={'div'} // default 'div'
      options={flickityOptions} // takes flickity options {}
      disableImagesLoaded={false} // default false
      reloadOnUpdate // default false
      static // default false
    >
        <img src={image1} alt=""/>
        <img src={image1} alt=""/>
        <img src={image1} alt=""/>




      
    </Flickity>
  )
}

const News = () => {
  return (
    <div className='news mb-5'>
      <div class="row justify-content-md-center mb-5">
         <div style={{ textAlign: 'center'}} class="col col-8">
            <p className='upcomingTitle mt-5'>News</p>
            <p style={{fontWeight: 400}}>Eclipse to the world of the end of our story Inside</p>

         </div>
      </div>

      <Carousel />

      <div style={{textAlign: 'center'}} className='row justify-content-md-center m-5'>
          <div className='col-3'>
            <div className='btn btn-primary px-4' >VIEW ALL</div>

          </div>


      </div>


      

      
    </div>
  )
}

export default News