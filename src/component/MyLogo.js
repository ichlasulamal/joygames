import React from 'react'
import logo from '../Group_99.svg'


const MyLogo = () => {
  return (
    <div style={{textAlign: 'center'}} className='navbar-brand row justify-content-md-center align-items-center '>
                        <div className='col'>
                            <img src={logo} alt="" width="80" height="54" class="d-inline-block align-text-top" />

                        </div>
                        <div className='col ' style={{marginLeft: '-20px'}} >
                            <p style={{margin: '0', fontWeight: '600'}}>Joy</p>
                            <p style={{margin: '0',  fontWeight: '900'}}>GAMES</p>

                        </div>
                    </div>
  )
}

export default MyLogo