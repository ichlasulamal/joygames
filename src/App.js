import './App.css';
import Footer from './component/Footer';
import Header from './component/Header';
import Jumbotron from './component/Jumbotron';
import Merchant from './component/Merchant';
import News from './component/News';
import Opening from './component/Opening';
import StrategicInvestor from './component/StrategicInvestor';
import Upcoming from './component/Upcoming';


function App() {
  return (
    <div className="App">
      <Header/>
      <Jumbotron/>
      <Merchant/>
      <Opening/>
      <Upcoming/>
      <News/>
      <StrategicInvestor />
      <Footer/>
    </div>
  );
}

export default App;
